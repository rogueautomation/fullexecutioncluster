# QA Automation Tools

## VM Inventory

- 10.222.128.30 => GGR-01
- 10.222.128.31 => SELENOID01-01
- 10.222.128.32 => SELENOID01-02
- 10.222.128.33 => Jenkins-01
- 10.222.128.34 => SELENOID01-03
- 10.222.128.12 => Jenkis-02
- 10.222.128.13 => GGR-02
- 10.222.128.14 => SELENOID02-04
- 10.222.128.15 => SELENOID02-05
- 10.222.128.16 => SELENOID02-06
- 10.66.172.150 => SELENOID03-07


Require to install the following software
- docker


## Install docker

Add the repository and intall the required packages
```bash
yum install nano
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install docker-ce docker-ce-cli containerd.io
```
Start the docker service and anable to always run in the startup
```
systemctl start docker
systemctl enable docker
```

Download the latest selenoid cm tool and rename it to `cm`
```
cd /
wget -c https://github.com/aerokube/cm/releases/download/1.8.1/cm_linux_amd64
mv cm_linux_amd64 cm 
```
Add execution permision to `cm` file.
```
chmod +x cm
```
### Browsers configuration

The browsers configuration is set into the `browsers.json` file, we use the following configuration:
```json
{
   "MicrosoftEdge": {
        "default": "91.0",
        "versions": {
            "93.0": {
                "image": "browsers/edge:93.0",
                "port": "4444",
                "path": "/"
            },
            "88.0": {
                "image": "browsers/edge:88.0",
                "port": "4444",
                "path": "/"
            },
            "91.0": {
                "image": "browsers/edge:91.0",
                "port": "4444",
                "path": "/"
            }
        }
    },
    "chrome": {
        "default": "85.0",
        "versions": {
            "84.0": {
                "image": "selenoid/chrome:84.0",
                "port": "4444",
                "path": "/"
            },
            "85.0": {
                "image": "selenoid/chrome:85.0",
                "port": "4444",
                "path": "/"
            },
            "91.0": {
                "image": "selenoid/chrome:91.0",
                "port": "4444",
                "path": "/"
            }
        }
    },
    "firefox": {
        "default": "80.0",
        "versions": {
            "79.0": {
                "image": "selenoid/firefox:79.0",
                "port": "4444",
                "path": "/wd/hub"
            },
            "80.0": {
                "image": "selenoid/firefox:80.0",
                "port": "4444",
                "path": "/wd/hub"
            },
            "90.0": {
                "image": "selenoid/firefox:90.0",
                "port": "4444",
                "path": "/wd/hub"
            }
        }
    },
    "opera": {
        "default": "70.0",
        "versions": {
            "69.0": {
                "image": "selenoid/opera:69.0",
                "port": "4444",
                "path": "/"
            },
            "70.0": {
                "image": "selenoid/opera:70.0",
                "port": "4444",
                "path": "/"
            }
        }
    }
}
```
Use the previos configuration into the default configuration file  that the 
cm tool use for selenoid.

```
mkdir -p  ~/.aerokube/selenoid/
nano -p  ~/.aerokube/selenoid/browsers.json
```
Then paste the configuration and hit `ctl+o` to save the file and then `ctl+x` to
close the nano editor.

### Run selenoid and selenoid-ui 

We use cd selnoid cm tool to run selenoid and selenoid-ui.
```
/cm selenoid start --args "--limit 16"
/cm selenoid-ui start
```
If we want to stop the selenoid or selenoidui instances we use the command
`/cm selenoid stop` for selenoid and `/cm selenoid-ui stop` for
selenoid-ui.

### Pull browsers

Selenoid uses docker images for each of the different browser versions, each browser version is
configured inot the `borwsers.json` file ther need to be available into the local docker image 
respository.
```
docker pull browsers/edge:93.0 &&\
docker pull browsers/edge:91.0 &&\
docker pull browsers/edge:88.0 &&\
docker pull selenoid/chrome:91.0 &&\
docker pull selenoid/chrome:85.0 &&\
docker pull selenoid/chrome:84.0 &&\
docker pull selenoid/firefox:90.0 &&\
docker pull selenoid/firefox:80.0 &&\
docker pull selenoid/firefox:79.0 &&\
docker pull selenoid/chrome:opera:70.0 &&\
docker pull selenoid/chrome:opera:69.0
```

## Runnin all with  Docker compose

We have created a `docker-compose.yml` file with all the requiered insfraestructure.
A network to be used for the selenoid must be creted using the followinf command:

```bash
docker network create selenoid
```

To run all the infraestructure, use the docker-compose command:

```bash
docker-compose up
```

To stop, perss `ctl+c` in the console, to delete all the created container, use the command:

```bash
docker-compose down
```
